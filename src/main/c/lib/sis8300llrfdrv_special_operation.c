/*
 * m-kmod-sis8300llrf
 * Copyright (C) 2014-2015  Cosylab

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/**
 * @file sis8300llrfdrv_special_operation.c
 * @brief Special operating modes for the LLRf controller
 */
#include <stdint.h>

#include <sis8300drv.h>
#include <sis8300drv_utils.h>
#include <sis8300_reg.h>

#include "sis8300llrfdrv.h"
#include "sis8300llrf_reg.h"
#include "sis8300llrfdrv_special_operation.h"


#define SIS8300LLRF_FSM_STATE_MASK            0x7
#define SIS8300LLRF_FSM_STATE_INIT            0
#define SIS8300LLRF_FSM_STATE_IDLE            1
#define SIS8300LLRF_FSM_STATE_PULSE_SETUP     2
#define SIS8300LLRF_FSM_STATE_ACTIVE_NO_PULSE 3
#define SIS8300LLRF_FSM_STATE_ACTIVE_PULSE    4
#define SIS8300LLRF_FSM_STATE_PULSE_END       5
#define SIS8300LLRF_FSM_STATE_PMS             6


/**
 * @brief Select operating mode for the controller
 *
 * @param [in] sisuser    User context Struct
 * @param [in] mode       Desired operating mode 
 *                        #sis8300llrfdrv_operating_mode
 *
 * @return status_success       Success
 * @return status_device_armed  Operatin is not allowed when the device
 *                              is armed
 * @return status_no_device     Device not opened
 * @return status_device_access Cannot access device registers
 *
 * This will setup a specific operating mode of device, by configuring 
 * the correct registers.
 *
 * Calls to this function are serialized with respect to other calls 
 * that alter the functionality of the device. This means that this 
 * function may block.
 */
int sis8300llrfdrv_set_operating_mode(
        sis8300drv_usr *sisuser, sis8300llrfdrv_operating_mode mode) {

    int status;
    sis8300drv_dev *sisdevice;

    uint32_t ui32_pi1_reg_val, ui32_pi2_reg_val, 
             ui32_vm_reg_val, ui32_iq_reg_val;

    sisdevice = sisuser->device;
    if (!sisdevice) {
        return status_no_device;
    }
    pthread_mutex_lock(&sisdevice->lock);

    if (sisdevice->armed) {
        pthread_mutex_unlock(&sisdevice->lock);
        return status_device_armed;
    }

    /* Current PI controller, VM output and IQ sampling values,
     * they are controlled trough four registers. Read the registers 
     * and than modify only the relevant bits to avoid messing up other
     * settings. */
    status = sis8300_reg_read(sisdevice->handle, 
                SIS8300LLRF_PI_1_CTRL_REG, &ui32_pi1_reg_val);
    if (status) {
        pthread_mutex_unlock(&sisdevice->lock);
        return status_device_access;
    }
    
    status = sis8300_reg_read(sisdevice->handle, 
                SIS8300LLRF_PI_2_CTRL_REG, &ui32_pi2_reg_val);
    if (status) {
        pthread_mutex_unlock(&sisdevice->lock);
        return status_device_access;
    }
    
    status = sis8300_reg_read(sisdevice->handle, 
                SIS8300LLRF_VM_CTRL_REG, &ui32_vm_reg_val);
    if (status) {
        pthread_mutex_unlock(&sisdevice->lock);
        return status_device_access;
    }
    
    status = sis8300_reg_read(sisdevice->handle, 
                SIS8300LLRF_IQ_CTRL_REG, &ui32_iq_reg_val);
    if (status) {
        pthread_mutex_unlock(&sisdevice->lock);
        return status_device_access;
    }

    /* First, set all the related settings to default*/
    ui32_pi1_reg_val &= ~(0xf << 2);    /* PI output select: Clear = normal */
    ui32_pi2_reg_val &= ~(0xf << 2);    /* PI output select: Clear = normal */

    ui32_iq_reg_val  &= ~(0x1 << 6);    /* Normal mode (disable freq. offset) */
    ui32_iq_reg_val  &= ~(0x1 << 5);    /* Measure phase reference = normal */

    ui32_vm_reg_val  &= ~(0x1 << 4);    /* disable force magnitude with SP and FF values */
    ui32_vm_reg_val  &= ~(0x1 << 5);    /* disable firce angle with SP and ff values */
/*
    This was removed because
    enabling or disabling the fixed point should be done separately,
    since modes can run with fixed or with tables
    ui32_pi1_reg_val &= ~(0x1 | 0x2);  // disable fixed SP and FF
    ui32_pi2_reg_val &= ~(0x1 | 0x2);  // disable fixed SP and FF
*/

    switch (mode) {
        case mode_normal:
            break;
        case mode_sig_gen_spff:
            ui32_pi1_reg_val |= 0x6 << 2;     /* PI output select: SP when no beam, FF when beam */
            ui32_pi2_reg_val |= 0x6 << 2;     /* PI output select: SP when no beam, FF when beam */
            break;
        case mode_sig_gen_sp:
            ui32_pi1_reg_val |= 0x2 << 2;     /* PI output select: SP value(s) */
            ui32_pi2_reg_val |= 0x2 << 2;     /* PI output select: SP value(s) */
            break;
        case mode_sig_gen_ff:
            ui32_pi1_reg_val |= 0x4 << 2;     /* PI output select: FF value(s) */
            ui32_pi2_reg_val |= 0x4 << 2;     /* PI output select: FF value(s) */
            break;
        case mode_mag_ctrl:
            /* I part takes role of magnitude = PI_1 */
            ui32_vm_reg_val  |= 0x1 << 4;     /* force mag to SP and FF values */
            break;
        case mode_ang_ctrl:
            /* Q part takes role of angle = PI_2 */
            ui32_vm_reg_val  |= 0x1 << 5;     /* force ang to SP and FF values */
            break;
        case mode_sel:
            ui32_pi1_reg_val |= 0x1 << 2;     /* PI output select: Output I value -> bypass PI */
            ui32_pi2_reg_val |= 0x1 << 2;     /* PI output select: Output Q value -> bypass PI */

            ui32_iq_reg_val  |= 0x1 << 5;     /* Force phase reference to zero */
            ui32_iq_reg_val  |= 0x1;          /* Rotate with angle offset */

            ui32_vm_reg_val  |= 0x1 << 4;     /* Force magnitude to SP and FF tables */
            break;
        case mode_freq_offset:
            ui32_pi1_reg_val |= 0x1 << 2;     /* Output source I: input to PI controller (open loop) */
            ui32_pi2_reg_val |= 0x1 << 2;     /* Output source Q: input to PI controller (open loop) */

            ui32_iq_reg_val  |= 0x1 << 6;     /* Select Freq. offset mode */
            ui32_iq_reg_val  |= 0x1;          /* Enable rotate with iq angle */
            break;
        default:
            pthread_mutex_unlock(&sisdevice->lock);
            return status_argument_invalid;
    }


    /* Write new PI controller, VM output and IQ sampling settings
     * to registers */
    
    status = sis8300_reg_write(sisdevice->handle, 
                SIS8300LLRF_PI_1_CTRL_REG, ui32_pi1_reg_val);
    if (status) {
        pthread_mutex_unlock(&sisdevice->lock);
        return status_device_access;
    }
    
    status = sis8300_reg_write(sisdevice->handle, 
                SIS8300LLRF_PI_2_CTRL_REG, ui32_pi2_reg_val);
    if (status) {
        pthread_mutex_unlock(&sisdevice->lock);
        return status_device_access;
    }
    
    status = sis8300_reg_write(sisdevice->handle, 
                SIS8300LLRF_VM_CTRL_REG, ui32_vm_reg_val);
    if (status) {
        pthread_mutex_unlock(&sisdevice->lock);
        return status_device_access;
    }
    
    status = sis8300_reg_write(sisdevice->handle, 
                SIS8300LLRF_IQ_CTRL_REG, ui32_iq_reg_val);
    if (status) {
        pthread_mutex_unlock(&sisdevice->lock);
        return status_device_access;
    }

    pthread_mutex_unlock(&sisdevice->lock);
    return status_success;
}

/**
 * @brief Will force a trigger. 
 *
 * @param [in] sisuser  User context struct
 * @param [in] trigger  Which trigger to force #sis8300llrfdrv_trigger
 *
 * @return status_success          success
 * @return status_device_access    Can't access device registers
 * @return status_no_device        Device not opened
 * @return status_argument_invalid Invalid choice of trigger type
 *
 * Be careful with the function. It is mostly meant to be used in 
 * special operating modes
 */
int sis8300llrfdrv_force_trigger(
        sis8300drv_usr *sisuser, sis8300llrfdrv_trigger trigger) {
    
    int status;
    uint32_t flag;
    sis8300drv_dev *sisdevice;

    sisdevice = sisuser->device;
    if (!sisdevice) {
        return status_no_device;
    }
    pthread_mutex_lock(&sisdevice->lock);

    switch(trigger) {
        case trig_pulse_comming:
            flag = 0x1 << 5;
            break;
        case trig_pulse_start:
            flag = 0x1 << 6;
            break;
        case trig_pulse_end:
            flag = 0x1 << 7;
            break;
        case trig_pms:
            flag = 0x1 << 8;
            break;
        case trig_update_params:
            /* We additionaly need to set the update parameters flag */
            status = sis8300_reg_write(sisdevice->handle, 
                    SIS8300LLRF_GIP_S_REG, SIS8300LLRFDRV_UPDATE_REASON_NEW_PARAMS);
            if (status) {
                pthread_mutex_unlock(&sisdevice->lock);
                return status_device_access;
            }
            flag = 0x1 << 9;
            break;
        default:
            pthread_mutex_unlock(&sisdevice->lock);
            return status_argument_invalid;
    }

    status = sis8300_reg_write(sisdevice->handle, 
                SIS8300LLRF_GIP_S_REG, flag);
    if (status) {
        pthread_mutex_unlock(&sisdevice->lock);
        return status_device_access;
    }
    
    pthread_mutex_unlock(&sisdevice->lock);
    return status_success;
}

/**
 * @brief   Check if the signal is active
 * @warning Only to be used in CW mode (special operation)
 * 
 * @param [in]  sisuser     Device user context struct
 * @param [out] is_active   On success this will be 1 = ACTIVE, 
 *                          0 = NOT ACTIVE
 * 
 * @return @see #sis3800drv_reg_read
 * 
 * When triggers are forced manually, the controller can operate in 
 * CW mode. This is NOT a normal operating mode and should not be used 
 * in normal operation. This function checks the firmware state machine 
 * to see if the controller is currently in active state. It should be 
 * used as information only and only when manually forcing the trggers. 
 * In all other cases infromation can be misleading!
 */
int sis8300llrfdrv_get_signal_active(
        sis8300drv_usr *sisuser, unsigned *is_active) {
    
    int status;
    
    status = sis8300drv_reg_read(sisuser, 
                SIS8300LLRF_GOP_REG, is_active);
    
    *is_active &= SIS8300LLRF_FSM_STATE_MASK;
    
    *is_active = (*is_active == SIS8300LLRF_FSM_STATE_ACTIVE_PULSE || 
                  *is_active == SIS8300LLRF_FSM_STATE_ACTIVE_NO_PULSE
                 ) ? 1 : 0; 
    
    return status;
}

static const uint32_t table_mode_addr[2] = {
    SIS8300LLRF_LUT_CTRL_FF_NSAMPLES_REG,
    SIS8300LLRF_LUT_CTRL_SP_NSAMPLES_REG,
    };

static const uint32_t table_mode_shift[2] = {
    16,
    13,
    };

/**
 * @brief   Set FF Table mode, circular or hold last
 *
 * @param [in] sisuser  User context Struct
 * @param [in] mode     Desired mode, #sis8300llrfdrv_table_mode
 *
 * @return status_success       Success
 * @return status_device_armed  Operatin is not allowed when the device
 *                              is armed
 * @return status_no_device     Device not opened
 * @return status_device_access Cannot access device registers
 *
 * Circular mode is only meant for special operating modes, so take care!
 *
 * Calls to this function are serialized with respect to other calls 
 * that alter the functionality of the device. This means that this 
 * function may block.
 */
int sis8300llrfdrv_set_table_mode(
        sis8300drv_usr *sisuser, 
        sis8300llrfdrv_ctrl_table ctrl_table,
        sis8300llrfdrv_table_mode mode) {
    
    int status;
    sis8300drv_dev *sisdevice;
    uint32_t ui32_reg_val;

    sisdevice = sisuser->device;
    if (!sisdevice) {
        return status_no_device;
    }
    pthread_mutex_lock(&sisdevice->lock);
/*
    In special operating modes this is allowed even if 
    the device is armed
    
    if (sisdevice->armed) {
        pthread_mutex_unlock(&sisdevice->lock);
        return status_device_armed;
    }
*/
    status = sis8300_reg_read(sisdevice->handle, 
                table_mode_addr[ctrl_table], &ui32_reg_val);
    if (status) {
        pthread_mutex_unlock(&sisdevice->lock);
        return status_device_access;
    }

    switch (mode) {
        case table_mode_normal:
            ui32_reg_val &= ~(0x1 << table_mode_shift[ctrl_table]);
            break;
        case table_mode_circular:
            ui32_reg_val |= 0x1 << table_mode_shift[ctrl_table];
            break;
        default:
            pthread_mutex_unlock(&sisdevice->lock);
            return status_argument_invalid;
    }

    status = sis8300_reg_write(sisdevice->handle, 
                table_mode_addr[ctrl_table], ui32_reg_val);
    if (status) {
        pthread_mutex_unlock(&sisdevice->lock);
        return status_device_access;
    }
    
    pthread_mutex_unlock(&sisdevice->lock);
    return status_success;
}

/**
 * @brief Get FF table mode
 *
 * @param [in]  sisuser  User context struct
 * @param [out] mode     will hold ff table mode on success
 *
 * @return status_success          Information retrieved successfully
 * @return status_device_access    Can't access device registers
 * @return status_no_device        Device not opened
 *
 * Returns the trigger setup that is currently used by the device.
 */
int sis8300llrfdrv_get_table_mode(
        sis8300drv_usr *sisuser, 
        sis8300llrfdrv_ctrl_table ctrl_table,
        sis8300llrfdrv_table_mode *mode) {
    
    int status;
    uint32_t ui32_reg_val;
    sis8300drv_dev *sisdevice;

    sisdevice = sisuser->device;
    if (!sisdevice) {
        return status_no_device;
    }

    status = sis8300_reg_read(sisdevice->handle, 
                table_mode_addr[ctrl_table], &ui32_reg_val);
    if (status) {
        return status_device_access;
    }

    *mode = (ui32_reg_val & (0x1 << table_mode_shift[ctrl_table]) ) ? 
                table_mode_circular : table_mode_normal;

    return status_success;
}

/**
 * @brief To be used in special operating modes
 * 
 * @param [in]  sisuser Device user context struct
 * 
 * @return status_no_device     Device not found
 * @return status_device_access Could not access device registers
 * @return status_success       Data retrieved successfully
 * 
 * This function allwos for unlocked arm of the board which is used 
 * during the setup procedure. It allows changing generic device 
 * settings (for example RTM attenuation) while the device is running. 
 * This is needed in order to be able to do the part of the setup that 
 * requires CW mode operation.
 */
int sis8300llrfdrv_arm_device_unlocked(sis8300drv_usr *sisuser) {
    
    int status;
    uint32_t ui32_reg_val;
    sis8300drv_dev *sisdevice;

    sisdevice = sisuser->device;
    if (!sisdevice) {
        return status_no_device;
    }

    /* Should not arm if there are pending operations on device. */
    pthread_mutex_lock(&sisdevice->lock);

    /* Reset sampling logic. */
    status = sis8300_reg_write(sisdevice->handle, 
                SIS8300_ACQUISITION_CONTROL_STATUS_REG, 
                SIS8300DRV_RESET_ACQ);
    if (status) {
        pthread_mutex_unlock(&sisdevice->lock);
        return status_device_access;
    }

    /* Wait until internal sampling logic is not busy anymore. */
    do {
        status = sis8300_reg_read(sisdevice->handle, 
                    SIS8300_ACQUISITION_CONTROL_STATUS_REG, &ui32_reg_val);
        if (status) {
            pthread_mutex_unlock(&sisdevice->lock);
            return status_device_access;
        }
    } while (ui32_reg_val & 0x30);

    status = sis8300_reg_write(sisdevice->handle, 
                SIS8300_ACQUISITION_CONTROL_STATUS_REG, 
                SIS8300DRV_TRG_ARM);
    if (status) {
        pthread_mutex_unlock(&sisdevice->lock);
        return status_device_access;
    }
    
    pthread_mutex_unlock(&sisdevice->lock);
    return status_success;
}
