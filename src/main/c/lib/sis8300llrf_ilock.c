#include <stdint.h>

#include <sis8300drv.h>
#include <sis8300drv_utils.h>
#include <sis8300_reg.h>

#include "sis8300llrfdrv.h"
#include "sis8300llrf_reg.h"

/**
 * @brief Set interlock condition for a harlink input
 *
 * @param [in] sisuser  Device user context struct
 * @param [in] harl_inp Harlink input number [0-3]
 * @param [in] trigg    Sensitivity, @see #sis8300llrfdrv_ilock
 *
 * @return status_success          Set successfull
 * @return status_device_access    Can't access device registers.
 * @return status_device_armed     This operation is not allowed on an 
 *                                 armed device.
 * @return status_no_device        Device not opened.
 * @return status_argument_invalid harl_inp or trigg choice is invalid
 *
 * Calls to this function are serialized with respect to other calls 
 * that alter the functionality of the device. This means that this 
 * function may block.
 */
int sis8300llrfdrv_set_ilock_condition(
        sis8300drv_usr *sisuser, 
        unsigned harl_inp, sis8300llrfdrv_ilock_condition condition) {

    int status;
    sis8300drv_dev *sisdevice;
    uint32_t ui32_reg_val_custom, ui32_reg_val_generic, flag;

    sisdevice = sisuser->device;
    if (!sisdevice) {
        return status_no_device;
    }
    pthread_mutex_lock(&sisdevice->lock);

    if (sisdevice->armed) {
        pthread_mutex_unlock(&sisdevice->lock);
        return status_device_armed;
    }

    if (harl_inp > (SIS8300DRV_NUM_HAR_CHANNELS - 1)) {
        pthread_mutex_unlock(&sisdevice->lock);
        return status_argument_invalid;
    }

    status = sis8300_reg_read(sisdevice->handle, 
                SIS8300LLRF_BOARD_SETUP_REG, &ui32_reg_val_custom);
    if (status) {
        pthread_mutex_unlock(&sisdevice->lock);
        return status_device_access;
    }

    status = sis8300_reg_read(sisdevice->handle, 
                SIS8300_HARLINK_IN_OUT_CONTROL_REG, &ui32_reg_val_generic);
    if (status) {
        pthread_mutex_unlock(&sisdevice->lock);
        return status_device_access;
    }

    flag = 0x1 << harl_inp;
    switch (condition) {
        case ilock_disabled:
            ui32_reg_val_generic &= ~(flag << 8);   /* disable edge trigger */
            ui32_reg_val_custom  &= ~(flag << 8);   /* disable level trigger */
            break;
        case ilock_rising_edge:
            ui32_reg_val_generic &= ~(flag << 12);  /* enable rising edge */
            ui32_reg_val_generic |=   flag << 8;    /* enable edge trigger */
            ui32_reg_val_custom  &= ~(flag << 8);   /* disable level trigger */
            break;
        case ilock_falling_edge:
            ui32_reg_val_generic |=   flag << 12;   /* enable falling edge */
            ui32_reg_val_generic |=   flag << 8;    /* enable edge trigger */
            ui32_reg_val_custom  &= ~(flag << 8);   /* disable level trigger */
            break;
        case ilock_high_level:
            ui32_reg_val_custom  &= ~(flag << 12);  /* enable high level */
            ui32_reg_val_generic &= ~(flag << 8);   /* disable edge trigger */
            ui32_reg_val_custom  |=   flag << 8;    /* enable level trigger */
            break;
        case ilock_low_level:
            ui32_reg_val_custom  |=   flag << 12;   /* enable low level */
            ui32_reg_val_generic &= ~(flag << 8);   /* disable edge trigger */
            ui32_reg_val_custom  |=   flag << 8;    /* enable level trigger */
            break;
        default:
            pthread_mutex_unlock(&sisdevice->lock);
            return status_argument_invalid;
    }

    status = sis8300_reg_write(sisdevice->handle, 
                SIS8300LLRF_BOARD_SETUP_REG, ui32_reg_val_custom);
    if (status) {
        pthread_mutex_unlock(&sisdevice->lock);
        return status_device_access;
    }

    status = sis8300_reg_write(sisdevice->handle, 
                SIS8300_HARLINK_IN_OUT_CONTROL_REG, ui32_reg_val_generic);
    if (status) {
        pthread_mutex_unlock(&sisdevice->lock);
        return status_device_access;
    }
    
    pthread_mutex_unlock(&sisdevice->lock);
    return status_success;
}

/**
 * @brief Get interlock condition of a harlink input
 *
 * @param [in]  sisuser  Device user context struct
 * @param [in]  harl_inp Harlink input number [0-3]
 * @param [out] trigg    Will contin ilock condition on success
 *
 * @return status_success          Data Retrieved successfully
 * @return status_device_access    Can't access device registers.
 * @return status_device_armed     This operation is not allowed on an 
 *                                 armed device.
 * @return status_no_device        Device not opened.
 * @return status_argument_invalid harl_inp is invalid
 *
 * Returns the ILOCK setup used by the device
 */
int sis8300llrfdrv_get_ilock_condition(
        sis8300drv_usr *sisuser, 
        unsigned harl_inp, sis8300llrfdrv_ilock_condition *condition) {
    
    int status;
    uint32_t ui32_reg_val, flag;
    sis8300drv_dev *sisdevice;

    sisdevice = sisuser->device;
    if (!sisdevice) {
        return status_no_device;
    }

    if (harl_inp > (SIS8300DRV_NUM_HAR_CHANNELS - 1)) {
        return status_argument_invalid;
    }

    flag = 0x1 << harl_inp;

    status = sis8300_reg_read(sisdevice->handle, 
                SIS8300LLRF_BOARD_SETUP_REG, &ui32_reg_val);
    if (status) {
        return status_device_access;
    }

    if (ui32_reg_val & (flag << 8) ) {
        /* level trigger is enabled */
        *condition = (ui32_reg_val & (flag << 12)) ?  
                            ilock_low_level : ilock_high_level;
    }
    else {
        status = sis8300_reg_read(sisdevice->handle, 
                    SIS8300_HARLINK_IN_OUT_CONTROL_REG, &ui32_reg_val);
        if (status) {
            return status_device_access;
        }
        if (ui32_reg_val & (flag << 8)) {
            /* edge trigger is enabled */
            *condition = (ui32_reg_val & (flag << 12)) ? 
                            ilock_falling_edge : ilock_rising_edge;
        }
        else {
            *condition = ilock_disabled;
        }
    }

    return status_success;
}


/**
 * @brief Get Local Protection System interlock 
 *
 * @param [in]  sisuser  Device user context struct
 * @param [out] ilock    Interlock enabled/disabled
 *
 * @return status_success          Data Retrieved successfully
 * @return status_device_access    Can't access device registers.
 * @return status_no_device        Device not opened.
 *
 * Returns the local protection system interlock
 */
int sis8300llrfdrv_get_lps_ilock(
        sis8300drv_usr *sisuser, unsigned *ilock) {
    
    int status;
    uint32_t ui32_reg_val;
    sis8300drv_dev *sisdevice;

    sisdevice = sisuser->device;
    if (!sisdevice) {
        return status_no_device;
    }

    status = sis8300_reg_read(sisdevice->handle, 
                SIS8300LLRF_GOP_REG, &ui32_reg_val);
    if (status) {
        return status_device_access;
    }

    *ilock = (ui32_reg_val >> 0x9) & 0x1;
    
    return status_success;
}


/**
 * @brief Reset Local Protection System interlock 
 *
 * @param [in]  sisuser  Device user context struct
 * @param [out] ilock    Interlock enabled/disabled
 *
 * @return status_success          Data Retrieved successfully
 * @return status_device_access    Can't access device registers.
 * @return status_no_device        Device not opened.
 *
 * Reset the Local Protection System Interlock
 */
int sis8300llrfdrv_reset_lps_ilock(
        sis8300drv_usr *sisuser) {
    
    int status;
    uint32_t ui32_reg_val;
    sis8300drv_dev *sisdevice;

    sisdevice = sisuser->device;
    if (!sisdevice) {
        return status_no_device;
    }


    status = sis8300_reg_write(sisdevice->handle, 
                SIS8300LLRF_LPS_ILOCK_RESET_1_REG, 0x7);
    if (status) {
        return status_device_access;
    }

    status = sis8300_reg_write(sisdevice->handle, 
                SIS8300LLRF_LPS_ILOCK_RESET_2_REG, 0x0);
    if (status) {
        return status_device_access;
    }
    
    return status_success;
}
