#include <stdint.h>

#include <sis8300drv.h>
#include <sis8300drv_utils.h>
#include <sis8300_reg.h>

#include "sis8300llrfdrv.h"
#include "sis8300llrf_reg.h"
#include "sis8300llrfdrv_types.h"

#define ROUNDUP_4K(val) ((unsigned) (val + 0xFFF) &~0xFFF)

/**
 * @brief Arm the device
 *
 * @param [in]  sisuser     User context struct
 *
 * @return status_success Arm successful
 * @return status_no_device Device not opened
 * @return status_device_access Cannot access device registers
 *
 * Overrides generic function, to make sure that trigg type ARM is always 
 * used. The llrf functionality ignores the SIS8300_SAMPLE_CONTROL_REG
 * completely. We want to make sure that it does not accidentally happen
 *
 * TODO: think on this
 */
int sis8300llrfdrv_arm_device(sis8300drv_usr *sisuser) {
    int status;
    uint32_t ui32_reg_val;
    sis8300drv_dev *sisdevice;

    sisdevice = sisuser->device;
    if (!sisdevice) {
        return status_no_device;
    }

    /* Should not arm if there are pending operations on device. */
    pthread_mutex_lock(&sisdevice->lock);

    if (!__sync_lock_test_and_set(&sisdevice->armed, 1)) {
        /* Reset sampling logic. */
        status = sis8300_reg_write(sisdevice->handle, 
                    SIS8300_ACQUISITION_CONTROL_STATUS_REG, 
                    SIS8300DRV_RESET_ACQ);
        if (status) {
                __sync_lock_release(&sisdevice->armed);
            pthread_mutex_unlock(&sisdevice->lock);
            return status_device_access;
        }

        /* Wait until internal sampling logic is not busy anymore. */
        do {
            status = sis8300_reg_read(sisdevice->handle, 
                        SIS8300_ACQUISITION_CONTROL_STATUS_REG, 
                        &ui32_reg_val);
            if (status) {
                    __sync_lock_release(&sisdevice->armed);
                pthread_mutex_unlock(&sisdevice->lock);
                return status_device_access;
            }
        } while (ui32_reg_val & 0x30);

        status = sis8300_reg_write(sisdevice->handle, 
                    SIS8300_ACQUISITION_CONTROL_STATUS_REG, 
                    SIS8300DRV_TRG_ARM);
        if (status) {
            __sync_lock_release(&sisdevice->armed);
            pthread_mutex_unlock(&sisdevice->lock);
            return status_device_access;
        }
    }

    pthread_mutex_unlock(&sisdevice->lock);
    return status_success;
}

/**
 * @brief wait for PULSE_DONE or PMS
 *
 * @param [in]  sisuser     User context struct
 * @param [in]  timeout     Wait irq timeout
 *
 * @return status_no_device     Device not opened
 * @return @see #sis8300drv_wait_irq
 *
 * The function will wait for the board to fire a user interrupt,
 * indicating PMS event or PULSE_DONE. This interrupt replaces DAQ DONE
 * interrupt on the generic version - it should not be counted on
 * here
 */
int sis8300llrfdrv_wait_pulse_done_pms(
        sis8300drv_usr *sisuser, unsigned timeout) {

    int status;
    sis8300drv_dev  *sisdevice;

    sisdevice = sisuser->device;
    if (!sisdevice) {
        return status_no_device;
    }

    status = sis8300drv_wait_irq(sisuser, irq_type_usr, timeout);

    __sync_lock_release(&sisdevice->armed);

    return status;
}

/**
 * @brief Set trigger setup for the controller
 *
 * @param [in]  sisuser     User context struct
 * @param [in]  trg_setup   @see #sis8300llrfdrv_trg_setup
 *
 * @return status_success          Information retrieved successfully
 * @return status_device_access    Can't access device registers
 * @return status_no_device        Device not opened
 *
 * Selects the trigger setup that should be used for this instance of 
 * the LLRF controller. The LLRF specific implementation ignores generic 
 * sis8300 trigger settings. This is the function that replaces them.
 *
 * Calls to this function are serialized with respect to other calls 
 * that alter the functionality of the device. This means that this 
 * function may block.
 */
int sis8300llrfdrv_set_trigger_setup(
        sis8300drv_usr *sisuser, sis8300llrfdrv_trg_setup trg_setup) {

    int status;
    sis8300drv_dev *sisdevice;
    uint32_t ui32_reg_val;

    sisdevice = sisuser->device;
    if (!sisdevice) {
        return status_no_device;
    }
    pthread_mutex_lock(&sisdevice->lock);

    if (sisdevice->armed) {
        pthread_mutex_unlock(&sisdevice->lock);
        return status_device_armed;
    }

    status = sis8300_reg_read(sisdevice->handle, 
                SIS8300LLRF_BOARD_SETUP_REG, &ui32_reg_val);
    if (status) {
        pthread_mutex_unlock(&sisdevice->lock);
        return status_device_access;
    }

    /* clear bits 0 and 1 */
    ui32_reg_val &= ~0x3;
    /* set the new value */
    ui32_reg_val |= (uint32_t) trg_setup & 0x3;

    status = sis8300_reg_write(sisdevice->handle, 
                SIS8300LLRF_BOARD_SETUP_REG, ui32_reg_val);
    if (status) {
        pthread_mutex_unlock(&sisdevice->lock);
        return status_device_access;
    }
    
    pthread_mutex_unlock(&sisdevice->lock);
    return status_success;
}

/**
 * @brief Get trigger setup for the controller
 *
 * @param [in]  sisuser     User context struct
 * @param [out] trg_setup   @see #sis8300llrfdrv_trg_setup
 *
 * @return status_success          Information retrieved successfully
 * @return status_device_access    Can't access device registers
 * @return status_no_device        Device not opened
 *
 * Returns the trigger setup that is currently used by the device.
 *
 */
int sis8300llrfdrv_get_trigger_setup(
        sis8300drv_usr *sisuser, sis8300llrfdrv_trg_setup *trg_setup) {

    int status;
    unsigned u_reg_val;

    status = sis8300drv_reg_read(sisuser, 
                SIS8300LLRF_BOARD_SETUP_REG, &u_reg_val);
    if (status) {
        return status_device_access;
    }

    u_reg_val &= 0x3;
    
    if (u_reg_val != (unsigned) mlvds_456) {
        /* values 0, 2 and 3 are mlvds_012 */
        *trg_setup = mlvds_012;
    }
    else {
        *trg_setup = mlvds_456;
    }

    return status_success;
}

/**
 * @brief get number of acquired samples during the passed pulse
 *
 * @param [in]  sisuser             User context struct
 * @param [in]  samples_cnt_source  see #sis8300llrfdrv_samples_cnt
 * @param [out] samples_Cnt_Val     Will heold the number of samples on 
 *                                  success
 *
 * @retval status_success Device successfully initialized.
 * @retval status_device_access Can't access device registers.
 * @retval status_no_device Device not opened.
 */
int sis8300llrfdrv_get_acquired_nsamples(
        sis8300drv_usr *sisuser, 
        sis8300llrfdrv_samples_cnt samples_cnt_source, 
        unsigned *samples_cnt_val) {
            
    uint32_t reg_addr;

    switch (samples_cnt_source) {
        case samples_cnt_pi_total:
            reg_addr = SIS8300LLRF_PI_ERR_CNT_R_REG;
            break;
        case samples_cnt_pi_ramp_up_phase:
            reg_addr = SIS8300LLRF_PULSE_START_CNT_R_REG;
            break;
        case samples_cnt_pi_active_phase:
            reg_addr = SIS8300LLRF_PULSE_ACTIVE_CNT_R_REG;
            break;
        case samples_cnt_cavity_total:
            reg_addr = SIS8300LLRF_SAMPLE_CNT_R_REG;
            break;
        default:
            return status_argument_range;
    }
    
    return sis8300drv_reg_read(sisuser, reg_addr, samples_cnt_val);
}


//TODO: add a function specific for internal and downsampled 
//and it is necessary to use the channel number
//because each channel will have a different register
/**
 * @brief get number of acquired samples for auxiliary channels during the 
 * passed pulse
 *
 * @param [in]  sisuser             User context struct
 * @param [in]  down_or_intern      Down-sampled or Internal
 * @param [out] samples_Cnt_Val     Will hold the number of samples on 
 *                                  success
 *
 * @retval status_success Device successfully initialized.
 * @retval status_device_access Can't access device registers.
 * @retval status_no_device Device not opened.
 */
int sis8300llrfdrv_get_acquired_nsamples_aux(
        sis8300drv_usr *sisuser, 
        int down_or_intern, 
        unsigned channel,
        unsigned *samples_cnt_val) {
    uint32_t reg_addr;
    int status;

    if (!down_or_intern)
        reg_addr = SIS8300LLRF_DWNSMPL_CH0_BYTE_COUNT_REG;
    else
        reg_addr = SIS8300LLRF_INTERN_CH0_BYTE_COUNT_REG;

    reg_addr += channel*SIS8300LLRF_AUX_CH_OFFSET;

    status = sis8300drv_reg_read(sisuser, 
            reg_addr, samples_cnt_val);

    *samples_cnt_val /= 4; //value in bytes, each sample is 4 bytes
    return status;
}

uint32_t addr_mag_avg[] = {
    SIS8300LLRF_SIGMON_CH0_AVG_REG, SIS8300LLRF_SIGMON_CH1_AVG_REG,
    SIS8300LLRF_SIGMON_CH2_AVG_REG, SIS8300LLRF_SIGMON_CH3_AVG_REG,
    SIS8300LLRF_SIGMON_CH4_AVG_REG, SIS8300LLRF_SIGMON_CH5_AVG_REG,
    SIS8300LLRF_SIGMON_CH6_AVG_REG, SIS8300LLRF_SIGMON_CH7_AVG_REG,
    SIS8300LLRF_SIGMON_CH8_AVG_REG, SIS8300LLRF_SIGMON_CH9_AVG_REG,
};

sis8300llrfdrv_Qmn Qmn_mag_avg = {.int_bits_m = 1, .frac_bits_n = 15, .is_signed = 0};

/**
 * @brief Get the average magnitude inside a configured window for a specific 
 * signal monitor AI channel
 * 
 * @param [in]  sisuser         User context struct
 * @param [in]  chan            AI channel to get the angle for
 * @param [out] avg_mag_val     Will contain the average magnitude on success
 *                              Will be 0 if the averaging has not finished
 *                              properly during last pulse
 * 
 * @return status_success           Data retrieved successfully
 * @return status_no_device         Device not found
 * @return status_device_access     Could not access device registers
 */ 

int sis8300llrfdrv_get_sigmon_mag_avg(
        sis8300drv_usr *sisuser, int chan, double *avg_mag_val) {
    int status;
	
	uint32_t ui32_reg_val;
    sis8300drv_dev *sisdevice;

    sisdevice = sisuser->device;
    if (!sisdevice) {
        return status_no_device;
    }
    
    status = sis8300_reg_read(sisdevice->handle, addr_mag_avg[chan], &ui32_reg_val);
    if (status) {
        return status_device_access;
    }
	ui32_reg_val &= 0xffff;
	
	sis8300llrfdrv_Qmn_2_double(ui32_reg_val, Qmn_mag_avg, avg_mag_val);
	
	return status_success;


}

uint32_t addr_refcomp_avg[] = {
    SIS8300LLRF_CAV_IQ_REG,
    SIS8300LLRF_REF_IQ_REG
};

sis8300llrfdrv_Qmn Qmn_refcomp_avg_iq = {.int_bits_m = 1, .frac_bits_n = 15, .is_signed = 1};

/**
 * @brief Get the average i andq inside a configured window for controller
 * input or reference line
 * 
 * @param [in]  sisuser         User context struct
 * @param [in]  ctrl_or_ref     0 for controller input 1 for reference line
 * @param [out] i               Will contain the average i component on success
 * @param [out] q               Will contain the average q component on success
 *                              Will be 0 if the averaging has not finished
 *                              properly during last pulse
 * 
 * @return status_success           Data retrieved successfully
 * @return status_no_device         Device not found
 * @return status_device_access     Could not access device registers
 */ 

int sis8300llrfdrv_get_refcomp_iq_avg(sis8300drv_usr *sisuser, 
        int ctrl_or_ref, double *i, double* q){
    int status;
	
	uint32_t ui32_reg_val, i_raw, q_raw;
    sis8300drv_dev *sisdevice;

    sisdevice = sisuser->device;
    if (!sisdevice) {
        return status_no_device;
    }
    
    status = sis8300_reg_read(sisdevice->handle, addr_refcomp_avg[ctrl_or_ref], &ui32_reg_val);
    if (status) {
        return status_device_access;
    }
	q_raw = ui32_reg_val & 0xffff;
    i_raw = (ui32_reg_val >> 16)  & 0xffff;
	
	sis8300llrfdrv_Qmn_2_double(i_raw, Qmn_refcomp_avg_iq, i);
	sis8300llrfdrv_Qmn_2_double(q_raw, Qmn_refcomp_avg_iq, q);
	
	return status_success;


}

/**
 * @brief Updates memory position for auxiliary channels 
 *  
 * @param [in]  sisuser         User context struct
 * 
 * @return status_success           Memory position set successfully
 * @return status_device_access     Can't access device registers.
 * @return status_no_device         Device not opened.
 * @return status_argument_range    Memory position outside memory limit
 * 
 * This function will set the memory position where firmware should record
 * the data from auxiliary channels. It will use
 * the last enabled raw channel final position as the start point, and will 
 * set the position for the  first internal channel. 
 * The downsampled channels will always be after the block of raw channels, so
 * if raw channel 4 is disabled but 6 is enabled, raw channel will start after
 * raw channel 6. The positions is aligned by 4K (0x1000).
 */ 

int sis8300llrfdrv_update_mem_position_aux_channel(sis8300drv_usr *sisuser){
    unsigned enabled, nsamples, i, raw_mask, nsamples_raw;
    int status;
    uint32_t next_position, raw_channel;
    sis8300drv_dev *sisdevice;
    double param_val;

    sisdevice = sisuser->device;
    if (!sisdevice) {
        return status_no_device;
    }

    //if is down-sampled, calculate next position from last raw channe 
    //enabled
    status = sis8300drv_get_channel_mask(sisuser, &raw_mask);
    if (status)
        return status;
    raw_channel = -1;
    //get the last enabled channel from raw channels
    for (i = 0; i < SIS8300DRV_NUM_AI_CHANNELS; i++)
        if (((raw_mask >> i) & 1) == 1)
            raw_channel = i;
    if (raw_channel != -1){ 
        status = sis8300drv_get_nsamples(sisuser, &nsamples_raw);
        //next position will be calculated considering the first raw channel 
        //will be at position 0x0 and is added a extra space of 1 block
        next_position = ROUNDUP_4K((raw_channel+1)*nsamples_raw*SIS8300DRV_SAMPLE_BYTES + SIS8300DRV_BLOCK_BYTES*8);
    }
    else
        next_position = 0; //there is no raw channel allocated

    if (next_position > sisdevice->adc_mem_size)
        return status_argument_range; //position outside memory limit


    //update the positions where data will be saved - Down-sampled channels
    for (i = 0; i < SIS8300DRV_NUM_AI_CHANNELS; i++){
    //check if the channel is enable (on firmware)
        status = sis8300llrfdrv_get_aux_param(0, sisuser, i, aux_param_enable, &param_val);
        if (status)
            return status;
        enabled = (unsigned) param_val;

        //if enable, get the number of set samples, and set the position of next channel to this 
        if (enabled) {
            //set the current position to next_position
             status = sis8300_reg_write(sisdevice->handle, 
                SIS8300LLRF_DWNSMPL_CH0_ADDR_REG + (i*SIS8300LLRF_AUX_CH_OFFSET), 
                next_position);
             if (status)
                return status;
            status = sis8300llrfdrv_get_aux_param(0, sisuser, i, aux_param_samples_cnt, &param_val);
            if (status)
                return status;
  
            nsamples = (unsigned) param_val;
            status = sis8300llrfdrv_get_aux_param(0, sisuser, i, aux_param_dec_enable, &param_val);
            if (status)
                return status;
            next_position += (nsamples * SIS8300LLRF_AUX_BYTES) + SIS8300DRV_BLOCK_BYTES*8;
            next_position = ROUNDUP_4K(next_position);
            if (next_position > sisdevice->adc_mem_size)
                return status_argument_range; //position outside memory limit

        }
    
    }

    //update the positions where data will be saved - Internal channels
    for (i = 0; i < SIS8300LLRFDRV_INTERN_CHANNELS; i++){
    //check if the channel is enable (on firmware)
        status = sis8300llrfdrv_get_aux_param(1, sisuser, i, aux_param_enable, &param_val);
        if (status)
            return status;
        enabled = (unsigned) param_val;

        //if enable, get the number of set samples, and set the position of next channel to this 
        if (enabled) {
            //set the current position to next_position
             status = sis8300_reg_write(sisdevice->handle, 
                SIS8300LLRF_INTERN_CH0_ADDR_REG + (i*SIS8300LLRF_AUX_CH_OFFSET), 
                next_position);
             if (status)
                return status;
            status = sis8300llrfdrv_get_aux_param(1, sisuser, i, aux_param_samples_cnt, &param_val);
            if (status)
                return status;
  
            nsamples = (unsigned) param_val;
            status = sis8300llrfdrv_get_aux_param(1, sisuser, i, aux_param_dec_enable, &param_val);
            if (status)
                return status;
            next_position += (nsamples * SIS8300LLRF_AUX_BYTES) + SIS8300DRV_BLOCK_BYTES*8;
            next_position = ROUNDUP_4K(next_position);
            if (next_position > sisdevice->adc_mem_size)
                return status_argument_range; //position outside memory limit

        }
    
    }

    return sis8300llrfdrv_update(sisuser, SIS8300LLRFDRV_UPDATE_REASON_NEW_PARAMS);
}

/**
 * @brief Read values from a specific auxiliary channel 
 *
 * @param [in]  sisuser     User context struct
 * @param [in]  channel     Channel number
 * @param [in]  down_or_intern   0: down-sampled channel
 *                               1: internal channel
 * @param [out] raw_data    Raw data from downsampled channel
 * @param [out] nbytes      Number of read bytes
 *
 * @return status_success           Data transfered successfully
 * @return status_device_access     Can't access device memory location 
 *                                  or device registers.
 * @return status_read       Can't transfer data from device 
 *                                  memory.
 * @return status_no_device         Device not opened.
 *
 * The function reads auxiliary channel raw data, as-is, from the memory. 
 * Data is in form of 32 bit samples.
 *
 */
int sis8300llrfdrv_read_aux_channel(
        sis8300drv_usr *sisuser, unsigned channel, unsigned down_or_intern, 
        void *raw_data, unsigned nsamples) {

    int status;
    uint32_t base_offset, reg_base;
    sis8300drv_dev *sisdevice;

    sisdevice = sisuser->device;
    if (!sisdevice) {
        return status_no_device;
    }
    pthread_mutex_lock(&sisdevice->lock);

    if (sisdevice->armed) {
        pthread_mutex_unlock(&sisdevice->lock);
        return status_device_armed;
    }

    if (!down_or_intern)
        reg_base = SIS8300LLRF_DWNSMPL_CH0_ADDR_REG;
    else
        reg_base = SIS8300LLRF_INTERN_CH0_ADDR_REG;

    status = sis8300_reg_read(sisdevice->handle, 
                reg_base + channel*SIS8300LLRF_AUX_CH_OFFSET,
                &base_offset);
    if (status) {
        pthread_mutex_unlock(&sisdevice->lock);
        return status_device_access;
    }
    status = sis8300drv_read_ram_unlocked(sisdevice, 
                base_offset, nsamples * SIS8300LLRF_AUX_BYTES, 
                raw_data);

    if (status) {
        pthread_mutex_unlock(&sisdevice->lock);
        return status_read;
    }

    pthread_mutex_unlock(&sisdevice->lock);
    return status;
}




/* ====================== INTERNAL LIBRARY FUNCTIONS ================ */
uint32_t addr_mag_ang[] = {
    SIS8300LLRF_SIGMON_CH0_MAG_ANG_REG, SIS8300LLRF_SIGMON_CH1_MAG_ANG_REG,
    SIS8300LLRF_SIGMON_CH2_MAG_ANG_REG, SIS8300LLRF_SIGMON_CH3_MAG_ANG_REG,
    SIS8300LLRF_SIGMON_CH4_MAG_ANG_REG, SIS8300LLRF_SIGMON_CH5_MAG_ANG_REG,
    SIS8300LLRF_SIGMON_CH6_MAG_ANG_REG, SIS8300LLRF_SIGMON_CH7_MAG_ANG_REG,
    SIS8300LLRF_SIGMON_CH8_MAG_ANG_REG, SIS8300LLRF_SIGMON_CH9_MAG_ANG_REG,
};

sis8300llrfdrv_Qmn Qmn_mag_ang[] = {
        {.int_bits_m = 1, .frac_bits_n = 15, .is_signed = 0},
        {.int_bits_m = 3, .frac_bits_n = 13, .is_signed = 1}};


/**
 * @brief Get the current magnitude and angle for a specific
 * channel
 * 
 * @param [in]  sisuser    User context struct
 * @param [in]  chan       AI channel to get the magnitude and angle
 * @param [out] mag        Will contain the magnitude value on success 
 * @param [out] ang        Will contain the angle value on success 
 *
 * @return status_success           Data retrieved successfully
 * @return status_no_device         Device not found
 * @return status_device_access     Could not access device registers
 *
 * The magnitude and angle are on the same register, so they will be read
 * at the same time to assure they are from the same pulse.
 */
int sis8300llrfdrv_get_sigmon_mag_ang(
                sis8300drv_usr *sisuser, 
                int chan, double *mag, double* ang) {
    int status;
	uint32_t ui32_reg_val, ui32_reg_ang;
    sis8300drv_dev *sisdevice;
    
    sisdevice = sisuser->device;
    if (!sisdevice) {
        return status_no_device;
    }
    
    status = sis8300_reg_read(sisdevice->handle, addr_mag_ang[chan], &ui32_reg_val);
    if (status) {
        return status_device_access;
    }
    ui32_reg_ang = ui32_reg_val;

    // calculate mag
	ui32_reg_val &= (0xffff);
	
	sis8300llrfdrv_Qmn_2_double(ui32_reg_val, Qmn_mag_ang[0], mag);

    // calculate ang
	ui32_reg_ang &= (0xffff << 16);
	ui32_reg_ang >>= 16;
	
	sis8300llrfdrv_Qmn_2_double(ui32_reg_ang, Qmn_mag_ang[1], ang);


	return status_success;
}
