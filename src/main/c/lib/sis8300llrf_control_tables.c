#include <stdint.h>

#include <sis8300drv.h>
#include <sis8300drv_utils.h>

#include "sis8300llrfdrv.h"
#include "sis8300llrf_reg.h"
#include "sis8300llrfdrv_types.h"

static inline int _sis8300llrfdrv_get_num_of_pulse_types(
                    sis8300drv_usr *sisuser, 
                    unsigned *num_pts);
static inline int _sis8300llrfdrv_get_rw_ctrl_table_params(
                    sis8300drv_usr *sisuser, 
                    sis8300llrfdrv_ctrl_table ctrl_table, 
                    unsigned pulse_type, 
                    uint32_t *base_table_offset, 
                    unsigned *max_nsamples);

/**
 * @brief Set desired pulse type
 *
 * @param [in] sisuser    User context struct
 * @param [in] pulse_type PT number
 *
 * @return status_success           On successfull set
 * @return status_argument_range    If pulse_type is out of range
 * @return status_device_access     Can't access device registers
 * @return status_no_device         Device not opened
 *
 *
 * The function sets pulse type to be used from now on. The function does
 * not check if FF and SP tables for the desired pulse type are already 
 * set, but just assumes that they lie at CT_OFFSET + PT * CT_SIZE. 
 * Whatever is there will be used.
 *
 * This is a shadow register. To make the controller see new parameters,
 * a call to #sis8300llrfdrv_update is needed.
 *
 * Calls to this function are serialized with respect to other calls 
 * that alter the functionality of the device. This means that this 
 * function may block.
 */
int sis8300llrfdrv_set_pulse_type(
        sis8300drv_usr *sisuser, unsigned pulse_type) {
    int status;
    unsigned num_pts;
    sis8300drv_dev *sisdevice;

    sisdevice = sisuser->device;
    if (!sisdevice) {
        return status_no_device;
    }
    pthread_mutex_lock(&sisdevice->lock);

    if (sisdevice->armed) {
        pthread_mutex_unlock(&sisdevice->lock);
        return status_device_armed;
    }

    status = _sis8300llrfdrv_get_num_of_pulse_types(sisuser, &num_pts);
    if (status) {
        pthread_mutex_unlock(&sisdevice->lock);
        return status;
    }
    if (pulse_type >= num_pts) {
        pthread_mutex_unlock(&sisdevice->lock);
        return status_argument_range;
    }

    status = sis8300_reg_write(sisdevice->handle,
                SIS8300LLRF_GIP_REG,
                ( ((uint32_t) pulse_type) << SIS8300LLRF_GIP_PT_SHIFT) 
                        & SIS8300LLRF_GIP_PT_MASK);
    if (status) {
        pthread_mutex_unlock(&sisdevice->lock);
        return status_device_access;
    }
    
    pthread_mutex_unlock(&sisdevice->lock);
    return status_success;
}

/**
 * @brief Get the currently active PT
 *
 * @param [in]  sisuser     User context struct
 * @param [out] pulse_type  Holds pulse_type number on success
 *
 * @return status_success       Information retrieved successfully
 * @return status_device_access Can't access device registers
 * @return status_no_device     Device not opened
 */
int sis8300llrfdrv_get_pulse_type(
        sis8300drv_usr *sisuser, unsigned *pulse_type) {

    int status;
    uint32_t ui32_reg_val;
    sis8300drv_dev *sisdevice;

    sisdevice = sisuser->device;
    if (!sisdevice) {
        return status_no_device;
    }

    status = sis8300_reg_read(sisdevice->handle, 
                SIS8300LLRF_GIP_REG, &ui32_reg_val);
    if (status) {
        return status_device_access;
    }

    *pulse_type = (unsigned) (
        (ui32_reg_val & SIS8300LLRF_GIP_PT_MASK) >> SIS8300LLRF_GIP_PT_SHIFT);

    return status_success;
}

/**
 * @brief Get the maximum allowed size of FF or SP table in 256 bit 
 *        chunks
 *
 * @param [in]  sisuser     User context struct
 * @param [in]  ctrl_table  Either ff or sp, #sis8300llrfdrv_ctrl_table
 * @param [out] table_size  Allowed table size -> number of 256 blocks
 *
 * @return status_success           Data retrieved successfully
 * @return status_device_access     Can't access device registers
 * @return status_no_device         Device not opened
 * @return status_argument_invalid  Wrong ctrl_table choice
 */
int sis8300llrfdrv_get_ctrl_table_max_nelm(
        sis8300drv_usr *sisuser, sis8300llrfdrv_ctrl_table ctrl_table, 
        unsigned *max_nelm) {

    int status;
    uint32_t ui32_reg_val;
    sis8300drv_dev *sisdevice;

    sisdevice = sisuser->device;
    if (!sisdevice) {
        return status_no_device;
    }

    /* read register */
    status = sis8300_reg_read(sisdevice->handle, 
                SIS8300LLRF_MEM_CTRL_CTRL_TABLE_SIZE_REG, &ui32_reg_val);
    if (status) {
        return status_device_access;
    }

    /* filter out the value */
    switch (ctrl_table) {
    case ctrl_table_sp:
        ui32_reg_val &= 0xffff0000; /* not actually necessary since we do a shift */
        ui32_reg_val >>= 16;
        break;
    case ctrl_table_ff:
        ui32_reg_val &= 0x0000ffff;
        break;
    default:
        return status_argument_invalid;
    }

    /* convert from size in 256 bit chunks (#SIS8300DRV_BLOCK_BYTES) 
     * to size in IQ samples of 16 bytes  (#SIS8300LLRF_IQ_SAMPLE_BYTES/2) 
     * for each table (16 for I and 16 for Q)*/
    *max_nelm = (unsigned) (ui32_reg_val * 
        (SIS8300LLRF_MEM_CTRL_BLOCK_BYTES / (SIS8300LLRF_IQ_SAMPLE_BYTES * 2)));

    return status_success;
}

/**
 * @brief Set number of samples for ff or sp table that is in use
 *        (that belong to current pulse_type)
 *
 * @param [in]  sisuser    User context struct
 * @param [in]  ctrl_table Either ff or sp, #sis8300llrfdrv_ctrl_table
 * @param [out] table_size Allowed table size -> number of 256 blocks
 *
 * @return status_success           Data retrieved successfully
 * @return status_device_access     Can't access device registers
 * @return status_argument_range    nsamples value is out of range
 * @return status_no_device         Device not opened
 * @return status_argument_invalid  Wrong ctrl_table choice
 *
 * nsamples can be between:
 *      0 and 2^12 - 1 (12 bits unsigned) for SP table
 *      0 and 2^15 - 1 (15 bits unsigned) for FF table
 *
 * Calls to this function are serialized with respect to other calls 
 * that alter the functionality of the device. This means that this 
 * function may block.
 */
int sis8300llrfdrv_set_ctrl_table_nelm(
        sis8300drv_usr *sisuser, sis8300llrfdrv_ctrl_table ctrl_table, 
        unsigned nelm) {

    int status;
    uint32_t ui32_reg_val = 0, base_offset = 0;
    sis8300drv_dev *sisdevice;

    sisdevice = sisuser->device;
    if (!sisdevice) {
        return status_no_device;
    }
    pthread_mutex_lock(&sisdevice->lock);

    /* get reg addr */
    switch (ctrl_table) {
    case ctrl_table_sp:
        base_offset = SIS8300LLRF_LUT_CTRL_SP_NSAMPLES_REG;
        break;
    case ctrl_table_ff:
        base_offset = SIS8300LLRF_LUT_CTRL_FF_NSAMPLES_REG;
        break;
    default:
        pthread_mutex_unlock(&sisdevice->lock);
        return status_argument_invalid;
    }

    /* check limits */

    unsigned max_elem;
    status = sis8300llrfdrv_get_ctrl_table_max_nelm(sisuser, ctrl_table, &max_elem);
    if (nelm > max_elem) {
        pthread_mutex_unlock(&sisdevice->lock);
        return status_argument_range;
    }

    /* read the current val to preserve ff table mode setting */
    status = sis8300_reg_read(sisdevice->handle, 
                base_offset, &ui32_reg_val);
    if (status) {
        pthread_mutex_unlock(&sisdevice->lock);
        return status_device_access;
    }

    /* set the new value */
    ui32_reg_val &= ~SIS8300LLRF_LUT_CTRL_NSAMPLES_MASK;
    ui32_reg_val |= ((uint32_t) nelm) & SIS8300LLRF_LUT_CTRL_NSAMPLES_MASK;

    /* write val */
    status = sis8300_reg_write(sisdevice->handle, 
                base_offset, ui32_reg_val);
    if (status) {
        pthread_mutex_unlock(&sisdevice->lock);
        return status_device_access;
    }
    
    pthread_mutex_unlock(&sisdevice->lock);
    return status_success;
}

/**
 * @brief Get number of samples for ff or sp table that is in use
 *        (that belong to current pulse_type)
 *
 * @param [in]  sisuser    User context struct
 * @param [in]  ctrl_table Either ff or sp, #sis8300llrfdrv_ctrl_table
 * @param [out] table_size Allowed table size -> number of 256 blocks
 *
 * @return status_success           Data retrieved successfully
 * @return status_device_access     Can't access device registers
 * @return status_no_device         Device not opened
 * @return status_argument_invalid  Wrong ctrl_table choice
 *
 * Samples are IQ points, 32 bit words consisting of 16 bit angle and 16
 * bit magnitude values
 */
int sis8300llrfdrv_get_ctrl_table_nelm(
        sis8300drv_usr *sisuser, sis8300llrfdrv_ctrl_table ctrl_table, 
        unsigned *nelm) {

    int status;
    uint32_t ui32_reg_val = 0, base_offset = 0;
    sis8300drv_dev *sisdevice;

    sisdevice = sisuser->device;
    if (!sisdevice) {
        return status_no_device;
    }

    /* get reg addr */
    switch (ctrl_table) {
    case ctrl_table_sp:
        base_offset = SIS8300LLRF_LUT_CTRL_SP_NSAMPLES_REG;
        break;
    case ctrl_table_ff:
        base_offset = SIS8300LLRF_LUT_CTRL_FF_NSAMPLES_REG;
        break;
    default:
        return status_argument_invalid;
    }

    /* read register */
    status = sis8300_reg_read(sisdevice->handle, 
                base_offset, &ui32_reg_val);
    if (status) {
        return status_device_access;
    }

    /* set value */
    *nelm = (unsigned) (ui32_reg_val & SIS8300LLRF_LUT_CTRL_NSAMPLES_MASK);

    return status_success;
}

/**
 * @brief Set the FF table speed - setting is the same for I and Q 
 *        controller
 *
 * @param [in]  sisuser    User context struct
 * @param [in]  speed      Can be betwen 0 and 15,
 *                         0 - 14 (every 1 to 15 clock cycle),
 *                         15 - every time a new PI  sample is available
 *
 * @return status_success           Data written successfully
 * @return status_device_access     Can't access device registers
 * @return status_argument_range    nsamples value is out of range
 * @return status_no_device         Device not opened
 *
 *
 * The function sets the rate at which next FF value is added to the 
 * PI-output. The setting is the same for both I and Q controller.
 *
 * Calls to this function are serialized with respect to other calls that 
 * alter the functionality of the device. This means that this function 
 * may block.
 */
int sis8300llrfdrv_set_ctrl_table_ff_speed(
        sis8300drv_usr *sisuser, unsigned speed) {

    int status;
    uint32_t ui32_reg_val;
    sis8300drv_dev *sisdevice;

    sisdevice = sisuser->device;
    if (!sisdevice) {
        return status_no_device;
    }
    pthread_mutex_lock(&sisdevice->lock);

    if (speed > 0xf) {
        pthread_mutex_unlock(&sisdevice->lock);
        return status_argument_range;
    }

    status = sis8300_reg_read(
                sisdevice->handle, 
                SIS8300LLRF_FF_TABLE_SPEED_REG, 
                &ui32_reg_val);
    if (status) {
        pthread_mutex_unlock(&sisdevice->lock);
        return status_device_access;
    }

    ui32_reg_val &= ~SIS8300LLRF_FF_TABLE_SPEED_MASK;
    ui32_reg_val |= ((uint32_t) speed) << SIS8300LLRF_FF_TABLE_SPEED_SHIFT;

    status = sis8300_reg_write(sisdevice->handle, 
                SIS8300LLRF_FF_TABLE_SPEED_REG, ui32_reg_val);
    if (status) {
        pthread_mutex_unlock(&sisdevice->lock);
        return status_device_access;
    }
    
    pthread_mutex_unlock(&sisdevice->lock);
    return status_success;
}
/**
 * @brief Get the FF table speed - setting is the same for I and Q 
 *        controller
 *
 * @param [in]  sisuser    User context struct
 * @param [out] speed      Can be betwen 0 and 15,
 *                         0 - 14 (every 1 to 15 clock cycle),
 *                         15 - every time a new PI  sample is available
 *
 * @return status_success           Data retrieved successfully
 * @return status_device_access     Can't access device registers
 * @return status_no_device         Device not opened
 *
 * The function sets the rate at which next FF value is added to the 
 * PI-output. The setting is the same for both I and Q controller.
 */
int sis8300llrfdrv_get_ctrl_table_ff_speed(
        sis8300drv_usr *sisuser, unsigned *speed) {

    int status;
    uint32_t ui32_reg_val;
    sis8300drv_dev *sisdevice;

    sisdevice = sisuser->device;
    if (!sisdevice) {
        return status_no_device;
    }

    status = sis8300_reg_read(sisdevice->handle, 
                SIS8300LLRF_FF_TABLE_SPEED_REG, &ui32_reg_val);
    if (status) {
        return status_device_access;
    }

    ui32_reg_val &= SIS8300LLRF_FF_TABLE_SPEED_MASK;
    *speed = (unsigned) (ui32_reg_val >> SIS8300LLRF_FF_TABLE_SPEED_SHIFT);

    return status_success;
}

/**
 * @brief Set the FF/SP table for given PT
 *
 * @param [in] sisuser      User context struct
 * @param [in] pulse_type   Pulse type that the table belongs to
 * @param [in] ctrl_table   Either FF or SP, #sis8300llrfdrv_ctrl_table
 * @param [in] table        The actual table to be written
 * @param [in] nsamples     Size of the table
 *
 * @return status_success           The table was written
 * @return status_device_access     Can't access device registers
 * @return status_no_device         Device not opened
 * @return status_argument_invalid  Wrong pulse_type choice
 * @return status_argument_range    Value out of bounds
 *
 * The function performs no checks on table, but just copies a chunk of 
 * size nsamples (settable by #sis8300llrfdrv_set_ctrl_table_nelm)
 * pointed to by table pointer to the memory for the selected pulse_type.
 *
 * The table consists of 32 bit elements, each element representing I 
 * and Q. The easiest way to handle the table is to interpret it as two
 * interleaved tables with 16 bit samples - one for I the other for Q,
 * where the first Q element is at offset 0 and I element at offset 1.
 * For conversion between raw 16 bit integers in the table and floats in
 * I and Q consult @see #sis8300llrfdrv_types.h
 *
 * This is a shadow register. To make the controller see new parameters,
 * a call to #sis8300llrfdrv_ctrl_table_update is needed.
 *
 * Calls to this function are serialized with respect to other calls that 
 * alter the functionality of the device. This means that this function 
 * may block.
 */
int sis8300llrfdrv_set_ctrl_table_raw(
        sis8300drv_usr *sisuser, sis8300llrfdrv_ctrl_table ctrl_table, 
        unsigned pulse_type, unsigned nelm, void *table_raw) {

    int status;
    uint32_t base_table_offset;
    unsigned max_nelm;
    sis8300drv_dev *sisdevice;

    sisdevice = sisuser->device;
    if (!sisdevice) {
        return status_no_device;
    }
    pthread_mutex_lock(&sisdevice->lock);

    status = _sis8300llrfdrv_get_rw_ctrl_table_params(sisuser, 
                ctrl_table, pulse_type, &base_table_offset, &max_nelm);
    if (status) {
        pthread_mutex_unlock(&sisdevice->lock);
        return status;
    }

    /* Check if the requested size is too big */
    if (nelm > max_nelm) {
        pthread_mutex_unlock(&sisdevice->lock);
        return status_argument_range;
    }

    /* Write the table to ram */
    status = sis8300drv_write_ram_unlocked(sisdevice,
                (unsigned) base_table_offset + 
                    max_nelm * SIS8300LLRF_IQ_SAMPLE_BYTES * pulse_type,
                nelm * SIS8300LLRF_IQ_SAMPLE_BYTES,
                table_raw);
    pthread_mutex_unlock(&sisdevice->lock);
    return status;
}

/**
 * @brief Read the control table from memory
 *
 * @param [in]  sisuser     User context struct
 * @param [in]  pulse_type  Pulse type that the table belongs to
 * @param [in]  ctrl_table  Either FF or SP, #sis8300llrfdrv_ctrl_table
 * @param [in]  nsamples    Size of the table to read
 * @param [out] table       Will contain the table on success
 *
 * @return status_success           Information retrieved successfully
 * @return status_device_access     Can't access device registers
 * @return status_no_device         Device not opened
 * @return status_argument_invalid  Wrong pulse_type choice
 *
 * Reads the SP or FF table for a given pulse type from memory. It 
 * assumes that the table passed to the function is of size TABLE_SIZE 
 * (specified with #sis8300llrfdrv_set_ctrl_table_nelm). It does not 
 * perform any checks.
 *
 * The table consists of 32 bit elements, each element representing Q
 * and I. The easiest way to handle the table is to interpret it as two
 * interleaved tables with 16 bit samples - one for Q the other for I,
 * where the first Q element is at offset 0 and I element at offset 1.
 * For conversion between raw 16 bit integers in the table and floats in
 * I and Q consult @see #sis8300llrfdrv_types.h
 *
 * Calls to this function are serialized with respect to other calls that 
 * alter the functionality of the device. This means that this function 
 * may block.
 */
int sis8300llrfdrv_get_ctrl_table_raw(
        sis8300drv_usr *sisuser, sis8300llrfdrv_ctrl_table ctrl_table, 
        unsigned pulse_type, unsigned nelm, void *table_raw) {

    int status;
    uint32_t base_table_offset;
    unsigned max_nelm;
    sis8300drv_dev *sisdevice;

    sisdevice = sisuser->device;
    if (!sisdevice) {
        return status_no_device;
    }
    pthread_mutex_lock(&sisdevice->lock);

    status = _sis8300llrfdrv_get_rw_ctrl_table_params(sisuser, 
                ctrl_table, pulse_type, &base_table_offset, &max_nelm);
    if (status) {
        pthread_mutex_unlock(&sisdevice->lock);
        return status;
    }

    /* If the requested size is bigger than max table size just return
     * a chunk of max size */
    if (nelm > max_nelm) {
        nelm = max_nelm;
    }

    /* Read the table */
    status = sis8300drv_read_ram_unlocked(sisdevice,
                (unsigned) base_table_offset + 
                    max_nelm * SIS8300LLRF_IQ_SAMPLE_BYTES * pulse_type,
                nelm * SIS8300LLRF_IQ_SAMPLE_BYTES,
                table_raw);

    pthread_mutex_unlock(&sisdevice->lock);
    return status;
}





/* =================== INTERNAL LIBRARY FUNCTIONS =================== */
/**
 * @brief Calculate maximum pulse type
 *
 * @param [in]  sisuser User context struct
 * @param [out] num_pts Will hold number of pulse types on success
 *
 * @return status_no_device     Device not opened
 * @return status_device_access Cannot access device registers
 * @return status_success       Max pulse type calculated
 *
 *
 * The function calculates maximum allowed pulse_type, based on control 
 * tables size from register .... and control table offset from register....
 *
 * This is an internal function, that allows check of pulse_type argument 
 * value. The function is not meant to be called from client applications.
 *
 *
 * CAREFULL HERE!!! WE DON'T WANT THE VALUES TO CHANGE IN BETWEEN
 *
 * This is an internal library function, that should not be called from
 * client code. It expects to be called from a function that has obtained 
 * the lock
 */
static inline int _sis8300llrfdrv_get_num_of_pulse_types(
                        sis8300drv_usr *sisuser, unsigned *num_pts) {

    int status;
    uint32_t ff_base, sp_base, pi_err_base, ff_end, ff_size;

    sis8300drv_dev *sisdevice = sisuser->device;
    if (!sisdevice) {
        return status_no_device;
    }

    status = sis8300_reg_read(sisdevice->handle, 
                SIS8300LLRF_MEM_CTRL_PI_ERR_BASE_REG, &pi_err_base);
    if (status) {
        return status_device_access;
    }

    status = sis8300_reg_read(sisdevice->handle, 
                SIS8300LLRF_MEM_CTRL_SP_BASE_REG, &sp_base);
    if (status) {
        return status_device_access;
    }

    status = sis8300_reg_read(sisdevice->handle, 
                SIS8300LLRF_MEM_CTRL_FF_BASE_REG, &ff_base);
    if (status) {
        return status_device_access;
    }

    /* make it as general as possible,
     * although if #sis8300llrfdrv_mem_ctrl_set_custom_mem_map is used,
     * than sp_base < ff_base < pi_err_base */
    if (sp_base < ff_base) {
        ff_end = (pi_err_base < ff_base) ? 
                    sisdevice->mem_size : pi_err_base;
    } 
    else if (pi_err_base < ff_base) {
        ff_end = (sp_base < ff_base) ? 
                    sisdevice->mem_size : sp_base;
    } 
    else {
        ff_end = (sp_base < pi_err_base) ? 
                    sp_base : pi_err_base;
    }

    status = sis8300llrfdrv_get_ctrl_table_max_nelm(sisuser, 
                ctrl_table_ff, &ff_size);
    if (status) {
        return status;
    }
    
    ff_size *= (uint32_t) SIS8300LLRF_IQ_SAMPLE_BYTES;
    *num_pts = (unsigned) ((ff_end - ff_base) / ff_size);

    return status_success;
}

/**
 * @brief Helper function to retrieve parameters for control table
 *
 * @param [in]  sisuser           User context struct
 * @param [in]  ctrl_table        Table type, #sis8300llrfdrv_ctrl_table
 * @param [in]  pulse_type        Table for which pulse type
 * @param [out] base_table_offset Will hold the offset of the table on 
 *                                success
 * @param [out] max_nsamples      Will hold max number of samples on 
 *                                success
 *
 * This is an internal library function, that should not be called from
 * client code.
 */
static inline int _sis8300llrfdrv_get_rw_ctrl_table_params(
                    sis8300drv_usr *sisuser, 
                    sis8300llrfdrv_ctrl_table ctrl_table,
                    unsigned pulse_type, uint32_t *base_table_offset, 
                    unsigned *max_nsamples) {

    int status;
    unsigned num_pts;
	sis8300drv_dev *sisdevice = sisuser->device;

    /* check if pulse type is valid */
    status = _sis8300llrfdrv_get_num_of_pulse_types(sisuser, &num_pts);
    if (status) {
        return status;
    }
    if (pulse_type >= num_pts) {
        return status_argument_invalid;
    }

    /* get base offset for control table */
    switch (ctrl_table) {
        case ctrl_table_sp:
            status = sis8300_reg_read(sisdevice->handle, 
                        SIS8300LLRF_MEM_CTRL_SP_BASE_REG, 
                        base_table_offset);
            if (status) {
                return status_device_access;
            }
            break;
        case ctrl_table_ff:
            status = sis8300_reg_read(sisdevice->handle, 
                        SIS8300LLRF_MEM_CTRL_FF_BASE_REG, 
                        base_table_offset);
            if (status) {
                return status_device_access;
            }
            break;
        default:
            return status_argument_invalid;
    }

    return sis8300llrfdrv_get_ctrl_table_max_nelm(sisuser, 
                ctrl_table, max_nsamples);
}
