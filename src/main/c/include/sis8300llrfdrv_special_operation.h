/*
 * m-kmod-sis8300llrf
 * Copyright (C) 2014-2015  Cosylab

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file sis8300llrfdrv_special_operation.h
 * @brief Header file for the sis8300 LLRF firmware special operating modes.
 *        To be used when operation other than normal (closed PI loop) 
 *        is required.
 */

#ifndef SIS8300LLRFDRV_SPECIAL_OPERATION_H_
#define SIS8300LLRFDRV_SPECIAL_OPERATION_H_

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief All triggers that can be forced
 * 
 * This is used during special operating modes and 
 * dusring the setup procedure. 
 * 
 * @warning The triggers should not be used in normal operation
 */
typedef enum {
	trig_pulse_comming 	= 0,    /** <Pulse comming event, use with parameter setup functions if required */
	trig_pulse_start	= 1,    /** <Pulse start event, use with parameter setup functions if required */
	trig_pulse_end		= 2,    /** <Pulse end event, use with parameter setup functions if required */
	trig_pulse_done		= 3,    /** <Pulse done event, use with parameter setup functions if required */
	trig_pms            = 4,    /** <PMS event */
	trig_update_params 	= 5	    /** <Force update of parameters now and don't wait for next pulse */
} sis8300llrfdrv_trigger;

/**
 * @brief All the operating modes available on the device
 * 
 * the modes can be used in CW and pulsed mode. 
 * 
 * For more information about special operating modes,
 * how they operate and their setup requirements consult
 * the firmware design document.
 * 
 * When the mode uses tables, the SP table is used to 
 * control the output between PULSE_COMMING and 
 * PULSE_START and FF table between PULSE_START and PULSE_END
 */
typedef enum {
    mode_sig_gen_spff     = 0,  /** < IQ signal generator mode: SP values when no beam, FF when beam*/
    mode_mag_ctrl         = 1,  /** < Magnitude output control */
    mode_ang_ctrl         = 2,  /** < Angle output control */
    mode_sel              = 3,  /** < Self excited Loop */
    mode_normal           = 4,  /** < Normal, closed loop mode */
    mode_sig_gen_sp       = 5,  /** < Output SP values */
    mode_sig_gen_ff       = 6,  /** < Output FF values */
    mode_freq_offset      = 7,  /** < Frequency offset mode: output freq. can be shifted up or down from the nominal RF freq. */
} sis8300llrfdrv_operating_mode;
int sis8300llrfdrv_set_operating_mode(sis8300drv_usr *sisuser, sis8300llrfdrv_operating_mode mode);

int sis8300llrfdrv_force_trigger(sis8300drv_usr *sisuser, sis8300llrfdrv_trigger trigger);
int sis8300llrfdrv_get_signal_active(sis8300drv_usr *sisuser, unsigned *is_active);

int sis8300llrfdrv_arm_device_unlocked(sis8300drv_usr *sisuser);

typedef enum {
    table_mode_normal   = 0,
    table_mode_circular = 1
} sis8300llrfdrv_table_mode;
int sis8300llrfdrv_set_table_mode(
        sis8300drv_usr *sisuser, 
        sis8300llrfdrv_ctrl_table ctrl_table,
        sis8300llrfdrv_table_mode mode);
int sis8300llrfdrv_get_table_mode(
        sis8300drv_usr *sisuser, 
        sis8300llrfdrv_ctrl_table ctrl_table,
        sis8300llrfdrv_table_mode *mode);

#ifdef __cplusplus
}
#endif

#endif /* SIS8300LLRFDRV_SPECIAL_OPERATION_H_ */
