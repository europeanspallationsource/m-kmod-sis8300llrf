PROJECT = sis8300llrfdrv

EXCLUDE_ARCHS += eldk

include $(EPICS_ENV_PATH)/module.Makefile

SOURCES += $(wildcard src/main/c/lib/*.c)
HEADERS += $(wildcard src/main/c/include/*.h) 
HEADERS += $(wildcard src/main/c/lib/*.h)


